import { Component, OnInit } from '@angular/core';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  nombre: string = '';

  constructor(public socketService:WebSocketService,private router:Router) { }

  ngOnInit() {
  }

  ingresar(){
    this.socketService.loginWS(this.nombre)
    .then(()=> this.router.navigateByUrl('/mensajes'));
  }

}
