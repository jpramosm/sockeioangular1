import { Injectable } from '@angular/core';
import { WebSocketService } from './web-socket.service';

@Injectable({
  providedIn: 'root'
})
export class ChatServiceService {

  constructor(private socketService: WebSocketService) { }

  sendmessage(mensaje: string) {

    const payload = {
      de: 'Fernando',
      cuerpo: mensaje,
    };

    this.socketService.emit('mensaje', payload);

  }

  getMessage() {
    return this.socketService.listen('mensaje-nuevo');
  }



}
