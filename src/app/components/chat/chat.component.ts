import { Component, OnInit } from '@angular/core';
import { ChatServiceService } from 'src/app/services/chat-service.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  texto = '';
  mensajes = [];

  constructor(private socketService:ChatServiceService) { }

  ngOnInit() {
    this.socketService.getMessage()
      .subscribe(msg => {
        console.log(msg);
        this.mensajes.push(msg)
      });
  }

  enviar() {
    this.socketService.sendmessage(this.texto);
  }

}
